## L'organisation des piles de cartes
Lorsqu'un joueur pioche des cartes, il le fait systématiquement **au sommet** de la pile. Dans ce projet, ces piles sont gérées avec des `ArrayList` et on considère que le sommet de la pile est la position 0 (voir par exemple `testPrendreCartesTransportPiocheWagonPiocheWagon` dans `JoueurProfTest` pour illustration).

# Début de partie

En début de partie chaque joueur doit choisir d'abord les destinations qu'il conserve parmi les 5 qui lui sont proposées, puis le nombre de pions wagon qu'il prend.

## Choisir les destinations

Pour choisir les destinations initiales, le jeu doit piocher 5 destinations et demander au joueur d'en défausser certaines s'il le souhaite. Le joueur peut alors envoyer les instructions suivantes :

- nom d'une destination à défausser
- "" pour passer (ne rien défausser)

lorsque le joueur a défaussé deux destinations, il prend automatiquement les 3 qui restent (pas besoin de passer)

Tests associés (dans `JeuProfTest`):
- `testDestinationsInitialesTousLesJoueursPassent`
- `testDestinationsInitialesJoueursDefaussent`

## Prendre des pions wagon

- entrer le nombre de pions wagon à prendre (les pions bateau sont automatiquement distribués selon les règles)

**Attention :** pour respecter les règles, le joueur doit prendre entre 10 et 25 pions wagon.

Tests associés (dans `JeuProfTest`):
- `testPionsWagonEtBateau`

# Tour du joueur

## Échanger des pions
- le joueur entre "_PIONS WAGON_" pour prendre des pions wagon de la réserve, ou "_PIONS BATEAU_" pour prendre des pions bateau (il faut qu'il puisse prendre au moins un pion du type demandé)
- ensuite le joueur entre le nombre de pions qu'il souhaite prendre (les autres pions sont ajustés selon les règles)

Tests associés (dans `JoueurProfTest`):
- `testPrendrePionsWagon`
- `testPrendrePionsBateau`

## Prendre des destinations
- le joueur entre "_DESTINATION_" pour piocher des destinations (la même chaîne de caractères est envoyée quand on clique sur la pile de destinations dans l'interface graphique)
- il peut ensuite entrer le nom d'une destination pour la défausser ou passer
- s'il ne reste plus qu'une seule destination, elle est automatiquement prise par le joueur (pas besoin de passer)

Tests associés (dans `JoueurProfTest`):
- `testPiocherDestinationsDefausser0`
- `testPiocherDestinationsDefausser2`
- `testPiocherDestinationsDefausser3`

## Capturer une route ou un port
- le joueur entre le nom de la route ou du port à capturer (c'est ce qui est envoyé quand on clique sur une route ou une ville de l'interface graphique)
- le joueur doit ensuite entrer les noms des cartes qu'il souhaite utiliser pour payer la route ou le port jusqu'à ce que la totalité du prix soit payé

Tests associés (dans `JoueurProfTest`) :
- `testCaptureRouteTerrestre`
- `testCaptureRouteTerrestreBis`
- `testCaptureRouteMaritime`
- `testCaptureRouteMaritimeGrise`
- `testCaptureRoutePaire`

## Piocher des cartes transport
- Pour piocher une carte wagon ou une carte bateau face cachée, le joueur entre "_WAGON_" ou "_BATEAU_" (c'est ce qui est envoyé quand on clique sur les piles de cartes wagon ou bateau de l'interface graphique)
- Pour piocher une carte transport face visible, le joueur entre le nom de la carte à piocher (c'est ce qui est envoyé si on clique sur la carte dans l'interface graphique). Ensuite, le joueur entre "_WAGON_" ou "_BATEAU_" pour choisir de remplacer la carte par une carte de la pioche wagon ou bateau respectivement. Même si une pioche est vide, il faut entrer le nom de l'autre pioche. Si par contre les deux pioches sont vides, le jeu passe automatiquement à la suite sans demander de choix au joueur.

Tests associés (dans `JoueurProfTest`) :
- `testPrendreCartesTransportPiocheWagonPiocheWagon`
- `testPrendreCartesTransportPiocheWagonPiocheBateau`
- `testPrendreCartesTransportPiocheBateauPasse`
- `testPrendreCartesTransportVisibleEnBateauVisibleEnWagon`
- `testPrendreCartesTransportVisibleEnWagonPiocheWagon`
- `testPrendreCartesTransportVisibleEnBateauPasse`
- `testPrendreCartesTransportJokerEnBateau`

**Remarque :** Le nom des cartes transport commence toujours par _C_ suivi d'un chiffre (voir le constructeur de `Jeu` et `CarteTransport`). En pratique, c'est plus simple d'utiliser l'interface graphique pour piocher des cartes. Mais si vous souhaitez afficher les noms des cartes visibles à la console, vous pouvez modifier la méthode `toString()` de `Jeu` et les afficher.


# Payer avec des cartes transport

Lorsqu'un joueur capture une route ou un port, il doit alors défausser des cartes transport pour en couvrir le prix. Le joueur doit envoyer les noms des cartes qu'il souhaite défausser en respectant les principes suivants :

- toutes les cartes défaussées doivent être utilisées dans le paiement de la route ou du port
- une carte ne peut être défaussée que s'il est possible de compléter avec des cartes que le joueur a en main
- dès que le prix est couvert le programme passe automatiquement à la suite
- les cartes défaussées ne peuvent pas être reprises (pas d'annulation en cours de paiement)

Par ailleurs, le joueur ne doit pas pouvoir commencer à payer une route ou un port s'il n'a pas les cartes suffisantes en main (le choix de la route ou du port ne doit pas être valide au début de son tour). Il est donc toujours possible de terminer de payer.

**Remarque :** Les cartes double bateau n'ont jamais de symbole "ancre". Elles ne peuvent donc jamais servir à payer un port. Comme elles ne servent pas non plus à payer des routes terrestres ou des routes paires (qui sont toutes des routes terrestres), elles n'interviennent que dans le paiement des routes maritimes.


## Exemples

### Exemple 1 (route terrestre colorée)

Le joueur a les cartes suivantes en main :
- Wagon *Rouge* (`C1`)
- Wagon *Rouge* (`C2`)
- Wagon *Vert* (`C3`)
- Wagon *Vert* (`C4`)
- Wagon *Jaune* (`C5`)
- *Joker* (`C6`)

Pour payer une route terrestre *rouge* de longueur 3, il essaie de jouer :

- "`C1`" (ok)
- "`C3`" (invalide, car ne sert pas à payer une route *rouge*)
- "`C2`" (ok)
- "`C6`" (ok, la route est payée)

Il a donc défaussé les cartes `C1`, `C2` et `C6`. Les autres cartes sont toujours dans sa main.


### Exemple 2 (route terrestre grise)

Le joueur a les mêmes cartes que précédemment en main. Pour payer une route terrestre *grise* de longueur 3, il essaie de jouer :

- "`C6`" (ok)
- "`C5`" (invalide, car pas possible de payer intégralement en *jaune*)
- "`C3`" (ok, mais il est maintenant obligé de finir en *vert*)
- "`C1`" (invalide, car déjà défaussé une carte *verte*)
- "`C4`" (ok, la route est payée)

Il a donc défaussé `C6`, `C3` et `C4`.


### Exemple 3 (route maritime)

Le joueur a en main les cartes suivantes :

- Bateau *Vert* (`C1`)
- Bateau *Vert* (`C2`)
- Double bateau *Vert* (`C4`)
- Double bateau *Vert* (`C3`)
- Double bateau *Blanc* (`C5`)
- *Joker* (`C6`)

Pour payer une route maritime *verte* de longueur 3, il essaie de jouer :

- "`C3`" (ok)
- "`C4`" (ok, les règles précisent bien qu'on peut dépasser le coût avec des cartes double bateau, et aucune carte n'est inutile)

Le joueur a donc défaussé les cartes `C3` et `C4`


### Exemple 4 (route maritime)

Le joueur a les mêmes cartes en main que dans l'exemple précédent. Pour payer une route maritime *verte* de longueur 4, il essaie de jouer :

- "`C1`" (ok)
- "`C3`" (ok)
- "`C4`" (invalide car si on accepte de défausser ces trois cartes la carte `C1` est inutile)
- "`C6`" (ok, la route est payée)

Il défausse donc les cartes `C1`, `C3` et `C6`.

**Remarque :** Le joueur aurait pû payer avec les cartes `C3` et `C4`, mais à partir du moment où il défausse la carte simple `C1` il ne peut plus "dépasser", puisque la carte `C1` serait alors inutile.


### Exemple 5 (route maritime)

Le joueur a les cartes suivantes en main :
- Bateau *Vert* (`C1`)
- Double bateau *Vert* (`C2`)
- Double bateau *Vert* (`C3`)
- Bateau *Blanc* (`C4`)

Pour payer une route maritime *verte* de longueur 4 il essaie de jouer :
- "`C1`" (invalide, car il n'est pas possible de payer exactement 4 avec les cartes qui lui restent en main, la carte `C1` sera donc forcément inutile à la fin du paiement)
- "`C2`" (ok)
- "`C3`" (ok)

Le joueur défausse donc les cartes `C2` et `C3`.


### Exemple 6 (route paire)

Le joueur a les cartes suivantes en main :
- Wagon *Rouge* (`C1`)
- Wagon *Rouge* (`C2`)
- Wagon *Rouge* (`C3`)
- Wagon *Rouge* (`C4`)
- Wagon *Vert* (`C5`)
- Wagon *Vert* (`C6`)
- Wagon *Violet* (`C7`)
- Wagon *Violet* (`C8`)
- Wagon *Jaune* (`C9`)

Pour payer une route paire de longueur 3, le joueur essaie de jouer :

- "`C1`" (ok début d'une paire *rouge*)
- "`C5`" (ok début d'une paire *verte*)
- "`C9`" (invalide, pas de paire *jaune* possible)
- "`C2`" (ok)
- "`C3`" (ok début d'une 2e paire *rouge*)
- "`C7`" (invalide, on ne peut pas commencer une 4e paire)
- "`C6`" (ok)
- "`C4`" (ok, la route est payée)

Le joueur défausse les cartes `C1`, `C2`, `C3`, `C4`, `C5` et `C6`.


### Exemple 7 (route paire)

Le joueur a les cartes suivantes en main :
- Wagon *Rouge* (`C1`)
- Wagon *Rouge* (`C2`)
- Wagon *Rouge* (`C3`)
- Wagon *Vert* (`C4`)
- Wagon *Violet* (`C5`)
- *Joker* (`C6`)

Pour payer une route paire de longueur 2, le joueur essaie de jouer :

- "`C5`" (ok, on peut finir la paire avec le *Joker*)
- "`C4`" (invalide, pas de paire *Verte* possible)
- "`C1`" (ok)
- "`C2`" (ok)
- "`C3`" (invalide, il faut compléter la paire *Violette*)
- "`C6`" (ok, la route est payée)

Le joueur défausse les cartes `C1`, `C2`, `C5` et `C6`.


# Score final

L'attribut `score` de la classe `Joueur` tient compte des points obtenus par la capture des routes (la valeur de chaque route dépend de sa longueur) et des points perdus lors des échanges de pions wagon et bateau. Cet attribut ne tient pas compte des points (positifs ou négatifs) liés à la réalisation des destinations ou aux ports.

Par contre, la méthode `calculerScoreFinal` de la classe `Joueur` doit renvoyer le score total du joueur en prenant en compte les destinations réalisées ou non (points positifs ou négatifs), la pénalité liée aux ports non construits et les points bonus des ports construits. Cependant, comme indiqué dans le sujet, pour les itinéraires (destination avec plus de deux villes) vous ne devez pas essayer de déterminer s'il est possible de relier les villes dans l'ordre. Le programme donnera simplement le bonus secondaire (pas la valeur maximale) si les villes sont connectées entre elles ou la pénalité si elles ne le sont pas.


# Choix uniques, choix automatiques

Dans certaines situations, le joueur n'a qu'une seule option valide parmi ses choix. Dans ce cas, il doit tout de même explicitement envoyer son choix.

Quelques exemples :

- si le joueur prend une carte transport face visible alors qu'il n'y a plus de cartes wagon à piocher (pile et défausse vides) le joueur doit tout de même envoyer `"BATEAU"` pour remplacer la carte visible par une carte de la pioche bateau (et il est obligé de le faire pour continuer)
- si le joueur doit payer une route terrestre rouge de longueur 2 et qu'il a exactement deux cartes wagon rouges en main et aucun joker, il doit tout de même envoyer les noms des deux cartes wagon rouges qu'il a en main (il peut choisir laquelle il défausse en premier, mais pour la seconde il n'a qu'une seule option)

Notez que, comme ça a été discuté dans [ce post](https://piazza.com/class/ld2tzi5k82via/post/41) sur Piazza, on considère que bien que ça ne soit pas ce qui est précisé dans les règles, le joueur a le droit de passer au début de son tour (il ne réalise aucune des 5 actions à son tour, on passe directement au tour du joueur suivant). On a donc également la situation suivante :

- si le joueur ne peut exécuter aucune des 5 actions au début de son tour (par exemple s'il n'a pas de carte transport en main, qu'il n'y a plus de cartes transport à piocher et qu'il ne peut plus échanger de pions parce qu'il n'a plus que des pions bateau après avoir posé ses 25 pions wagon sur le plateau) alors il doit explicitement passer son tour en envoyant la chaîne vide `""`

Toutefois, si le joueur est dans une situation où il devrait normalement faire un choix, mais qu'aucun des choix qu'il devrait faire n'est disponible, alors le jeu passe automatiquement à la suite sans lui demander d'envoyer un *input*. A priori la seule situation où cela se produit est dans le cas où le joueur prend une carte transport face visible alors qu'il n'y a plus aucune carte wagon ni bateau disponible à piocher. Dans ce cas, le jeu passe automatiquement sans lui demander de remplacer la carte (ni "WAGON" ni "BATEAU" ne sont des choix valides, et normalement dans cette situation il ne devrait pas avoir le droit de passer).

Dans tous les autres cas de figure le joueur devrait avoir au moins une option valide.

# Quelques explications supplémentaires 

## Choisir une destination

Lorsqu'un joueur doit choisir une destination il doit choisir en envoyant au jeu la chaîne de caractères correspondant à l'attribut `nom` de la destination.

Chaque destination créée a un attribut `nom` qui est unique et défini automatiquement par le constructeur. C'est une chaîne de caractères contenant la lettre `D` suivie d'un numéro unique : le nom de la première destination créée est "`D1`", le nom de la seconde est "`D2`", et ainsi de suite jusqu'à la dernière destination qui a le nom "`D65`" (vous pouvez aller voir le constructeur de `Destination` qui utilise une variable statique `compteur` incrémentée à chaque fois que le constructeur est appelé).

Lorsque vous voulez faire choisir une destination à un joueur parmi une liste de choix possibles, vous pouvez créer un `Bouton` pour chaque destination, dont l'attribut `label` est une description de la destination (vous pouvez utiliser la méthode `toString` de `Destination`) et l'attribut `valeur` est le nom de la destination (pour un bouton, le `label` est le texte qui sera affiché sur le bouton dans l'interface graphique, tandis que la `valeur` est ce qui sera envoyé au jeu si on clique sur le bouton).

Par exemple, si vous voulez faire choisir une destination parmi les 5 premières destinations de la pioche, vous pouvez utiliser un code similaire à ceci :
```java
// préparer la liste des destinations parmi lesquelles le joueur doit choisir
List<Destination> destinationsPiochees = new ArrayList<>();
for (int i = 0; i < 5; i++) {
    Destination d = jeu.piocherDestination();   // <- méthode à écrire
    destinationsPiochees.add(d);
}

// préparer les boutons pour que le joueur choisisse
List<Bouton> boutons = new ArrayList<>();
for (Destination d: destinationsPiochees) {
    boutons.add(new Bouton(d.toString(), d.getNom()));
}
// faire choisir le joueur en cliquant sur un bouton
String choix = choisir(
    "Choisissez une destination à défausser",
    null, // pas de choix en dehors des boutons
    boutons,
    true); // le joueur peut éventuellement passer

// ici la variable choix est soit "" si le joueur a passé, soit le nom d'une des
// destinations de `destinationsPiochees`
if (choix == "") {
    // ...
    // faire quelque chose si le joueur passe
    // ...
} else {
    // trouver la destination dont le nom est `choix`
    Destination destinationChoisie;
    for (Destination d: destinationsPiochees) {
        if (d.getNom().equals(choix)) {
            destinationChoisie = d; // on a trouvé la destination choisie
            break;
        }
    }
    
    // ...
    // faire quelque chose avec la destination choisie par le joueur
    // ...
}
```

## Choisir une route ou une carte transport

Les routes et les cartes transport ont également un nom unique généré automatiquement par le constructeur de `Route` ou de `CarteTransport` (et donc par les constructeurs de chacune de leurs sous-classes).

Le nom d'une carte transport est composé de la lettre `C` suivie du numéro de la carte (de "`C1`" jusqu'à "`C140`" créees dans le constructeur de `Jeu`). Le nom d'une route est composé de la lettre `R` suivie du numéro de la route (de "`R1`" jusqu'à "`R130`", cf. fichier `data/Plateau.java` pour la liste complète).

À la différence des destinations, **il n'est pas nécessaire de faire des boutons pour qu'un joueur choisisse une route ou une carte transport**. En effet, lorsque l'utilisateur clique sur les segments d'une route sur la carte du monde dans l'interface graphique le jeu reçoit le nom de la route (par exemple lorsqu'on clique sur la route blanche entre Marseille et Edinburgh, le jeu reçoit la chaîne de caractères "`R68`"). De même si l'on clique sur une carte de l'interface graphique (une carte à piocher face visible ou une carte dans la main d'un joueur) l'interface envoie le nom de la carte au jeu.

Ainsi, si on veut faire choisir une carte parmi les cartes wagon rouges ou les jokers qui sont dans la main du joueur, on peut écrire un code similaire à ceci (dans la classe `Joueur`):
```java
// créer la liste des cartes qu'on peut choisir
List<CarteTransport> optionsCartes = new ArrayList<>();
for (CarteTransport c: cartesTransport) {
    if (c.getType() == TypeCarteTransport.JOKER || 
    (c.getType() == TypeCarteTransport.WAGON && c.getCouleur() == Couleur.ROUGE)) {
        optionsCartes.add(c);
    }
}
// créer la liste des noms des cartes qu'on peut choisir
List<String> nomsOptionsCartes = new ArrayList<>();
for (CarteTransport c: optionsCartes) {
    nomsOptionsCartes.add(c.getNom());
}

// choisir le nom d'une carte
String choix = choisir(
    "Choisissez une carte à défausser",
    nomsOptionsCartes,
    null, // aucun bouton
    false); // le joueur n'a pas le droit de passer

// ici la fonction choisir renverra le nom de la première carte wagon rouge ou joker
// de la main du joueur sur laquelle l'utilisateur cliquera (les noms des autres cartes
// ne seront pas acceptés)
CarteTransport carteChoisie;
for (CarteTransport c: optionsCartes) {
    if (c.getNom().equals(choix)) {
        carteChoisie = c;   // on a trouvé la carte dont le nom a été choisi
        break;
    }
}
// ...
// faire quelque chose avec la carte choisie par le joueur
// ...
```

## Combiner des choix

En utilisant la même structure, on peut tout à fait faire choisir à l'utilisateur un élément parmi plusieurs ensembles contenant des objets de types différents.

Par exemple, si on veut faire choisir à l'utilisateur soit une carte transport parmi les cartes face visible du jeu, soit "`BATEAU`", soit "`WAGON`" soit une route parmi celles que le joueur peut capturer on peut utiliser un code similaire à ceci (dans la classe `Joueur`):

```java
List<String> choixPossibles = new ArrayList<>();
choixPossibles.add("BATEAU");
choixPossibles.add("WAGON");

List<String> nomsCartesTransportVisibles = new ArrayList<>();
for (CarteTransport c: jeu.getCartesTransportVisibles()) {
    nomsCartesTransportVisibles.add(c.getNom());
}
choixPossibles.addAll(nomsCartesTransportVisibles);

List<String> nomsRoutesCapturables = new ArrayList<>();
for (Route r: jeu.getRoutesLibres()) {
    if (peutCapturer(r)) { // <- méthode à écrire
        nomsRoutesCapturables.add(r.getNom());
    }
}
choixPossibles.addAll(nomsRoutesCapturables);

String choix = choisir(
    "Choisissez une carte à piocher ou une route à capturer",
    choixPossibles,
    null,
    false);

if (choix.equals("BATEAU")) {
    // ...
    // faire piocher la première carte de la pile bateau
    // ...
} else if (choix.equals("WAGON")) {
    // ...
    // faire piocher la première carte de la pile wagon
    // ...
} else if (nomsCartesTransportVisibles.contains(choix)) {
    CarteTransport carteChoisie;
    for (CarteTransport c: jeu.getCartesTransportVisibles()) {
        if (c.getNom().equals(choix)) {
            carteChoisie = c;
            break;
        }
        // ...
        // faire piocher `carteChoisie` au joueur
        // ...
    }
} else if (nomsRoutesCapturables.contains(choix)) {
    Route routeChoisie;
    for (Route r: jeu.getRoutesLibres()) {
        if (r.getNom().equals(choix)) {
            routeChoisie = r;
            break;
        }
    } 
    // ...
    // faire capturer la route `routeChoisie` au joueur
    // ...
}
```

**Remarque :**
- Lorsque l'utilisateur clique sur la carte face cachée qui représente la pioche de cartes wagon l'interface envoie "`WAGON`".
- Lorsque l'utilisateur clique sur la carte face cachée qui représente la pioche de cartes bateau l'interface envoie "`BATEAU`".
- Lorsque l'utilisateur clique sur la carte face cachée qui représente la pioche de cartes destinations l'interface envoie "`DESTINATION`".
